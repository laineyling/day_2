// 由于vue-2.5.21.js 文件（vue的核心库文件）对外暴露了Vue函数，所以就可以直接使用Vue函数实例化对象

// 实例化Vue对象，并挂载对象到app节点
var vm = new Vue({
    el:'#app',
    data:{//data  负责定义数据，内部定义的数据都是响应式数据，数据会自动添加在Vue实例对象身上
        title:'今天开始学习vuejs',
        count:888,
        color:'red'
    }
})

// 在Vue实例的外面  获取实例中定义的数据
// vm.title
// vm.$data.title

// 在Vue实例的外面，修改实例中定义的数据，修改响应式数据的同时，任何绑定了该数据的地方都会自动更新（程序员只需要提前绑定数据到页面，操作数据就可以了，页面的更新交给vuejs完成）
// vm.title = 'vue.js'