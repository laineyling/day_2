var vm = new Vue({
    el:'#like',
    data:{
        currentIndex:0,
        list:['综合排序','距离最近','销量最高','筛选'],
        type:['年货节热卖','津贴联盟','满减优惠','品质联盟'],
        arr:[
            {img:"pic.png",title:"金百万烤鸭",num:500,price:15,price2:3,time:47,distance:3.9},
            {img:"pic.png",title:"稻香金源饺子馆",num:230,price:20,price2:3.5,time:36,distance:3.5},
            {img:"pic.png",title:"麦当劳",num:500,price:15,price2:3,time:30,distance:3.6},
            {img:"pic.png",title:"肯德基",num:230,price:20,price2:3.5,time:32,distance:3.4},
            {img:"pic.png",title:"华莱士",num:500,price:15,price2:30,time:40,distance:2.8},
        ],
    },
    methods:{
        handleClick(index){
            this.currentIndex = index;//保存点击的按钮下标
            if(index == 1){
                this.arr.sort((a,b)=>{
                    return a.distance - b.distance;
                })
            }
            else if(index == 2){
                this.arr.sort((a,b)=>{
                    return b.num - a.num;
                })
            }
        },
    }
})