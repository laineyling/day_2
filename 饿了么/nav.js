new Vue({
    el:'#app',
    data:{
        currentIndex:0,//保存点击的按钮下标
        list:[
            { pic:'',name:'首页'},
            { pic:'',name:'订单'},
            { pic:'',name:'我的'},
        ]
    },
    methods:{
        handleClick(index){
            this.currentIndex = index;//保存点击的按钮下标
        }
    }
})