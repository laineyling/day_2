var vm = new Vue({
    el:"#app",
    data:{
        searchText: '',
        searchArr:['三文鱼','刺身','鲍鱼']
    },
    methods: {
        search() {
            if (this.searchText) {
                this.searchArr.push(this.searchText);
                this.searchText = '';
            }
        }
    }
})